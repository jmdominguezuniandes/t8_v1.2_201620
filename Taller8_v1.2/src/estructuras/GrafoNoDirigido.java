package estructuras;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Clase que representa un grafo no dirigido con pesos en los arcos.
 * @param K tipo del identificador de los vertices (Comparable)
 * @param E tipo de la informacion asociada a los arcos

 */
public class GrafoNoDirigido<K extends Comparable<K>, E> implements Grafo<K,E> {

	private int numNodos;
	private int numArcos;
	/**
	 * Nodos del grafo
	 */
	//TODO Declare la estructura que va a contener los nodos
	private Nodo[] nodos;

	/**
	 * Lista de adyacencia 
	 */
	private TablaHash<K,Lista> adj;
	//TODO Utilice sus propias estructuras (defina una representacion para el grafo)
		// Es libre de implementarlo con la representacion de su agrado. 

	/**
	 * Construye un grafo no dirigido vacio.
	 */
	public GrafoNoDirigido() {
		//TODO implementar
		nodos = new Nodo[0];
		numNodos = 0;
		numArcos = 0;
		adj = new TablaHash();
	}
	public int darNumeroNodos(){
		return numNodos;
	}

	@Override
	public boolean agregarNodo(Nodo<K> nodo) {
		//TODO implementar
		try{
			nodos[numNodos] = nodo;
			numNodos ++;
			return true;
			}
			catch(Exception e){
				return false;
			}
	}

	@Override
	public boolean eliminarNodo(K id) {
		//TODO implementar
		boolean encontrado = false;
		for(int i = 0; i < nodos.length ; i++){
			if(nodos[i].darId() == id){
				nodos[i] = null;
				encontrado = true;
			}
		}
		if(encontrado == true){
			int nuevoNumNodos = numNodos - 1;
			resizeNodos(nuevoNumNodos);
			numNodos --;
		}
		return encontrado;
	}
	
	private void resizeNodos(int pNumNodos) {
		// TODO Auto-generated method stub
		Nodo[] nuevoArreglo = new Nodo[pNumNodos];
		int j = 0;
		for(int i = 0; i < nodos.length; i++){
			if(nodos[i] != null){
				nuevoArreglo[j] = nodos[i];
				j++;
			}
		}
		nodos = nuevoArreglo;
		numNodos = pNumNodos;
		
	}

	@Override
	public Arco<K,E>[] darArcos() {
		//TODO implementar
		Lista lista = new Lista();
		for(int i = 0; i < nodos.length; i++){
			Lista temp = adj.get( (K) nodos[i].darId());
			if(temp != null){
				for(int j = 0; j < temp.getSize(); j++){
					lista.add(temp.get(j));
				}
			}
		}
		
		Arco[] respuesta = new Arco[lista.getSize()];
		for(int i = 0; i < lista.getSize(); i++){
			respuesta[i] = (Arco) lista.get(i);
		}
		
		return respuesta;
	}

	private Arco<K,E> crearArco( K inicio, K fin, double costo, E e )
	{
		Nodo<K> nodoI = buscarNodo(inicio);
		Nodo<K> nodoF = buscarNodo(fin);
		if ( nodoI != null && nodoF != null)
		{
			return new Arco<K,E>( nodoI, nodoF, costo, e);
		}
		else
		{
			return null;
		}
	}

	@Override
	public Nodo<K>[] darNodos() {
		//TODO implementar
		return nodos;
	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo, E obj) {
		//TODO implementar
		try{
			Arco nuevoArco = crearArco(inicio,fin,costo,obj);
			Lista lista = adj.get(inicio);
			Lista lista2 = adj.get(fin);
			if(lista != null){
				lista.add(nuevoArco);
				adj.put(inicio, lista);
			}
			else {
				lista = new Lista();
				lista.add(nuevoArco);
				adj.put(inicio,lista);
			}

			if(lista2 != null){
				lista2.add(nuevoArco);
				adj.put(fin, lista2);
			}
			else {
				lista2 = new Lista();
				lista2.add(nuevoArco);
				adj.put(fin,lista2);
			}
			numArcos ++;
			return true;
		}
		catch(Exception e){
			return false;
		}

	}

	@Override
	public boolean agregarArco(K inicio, K fin, double costo) {
		return agregarArco(inicio, fin, costo, null);
	}

	@Override
	public Arco<K,E> eliminarArco(K inicio, K fin) {
		//TODO implementar
		//TODO implementar
		Arco respuesta = null;
		Lista list = adj.get(inicio);
		int posicion = -1;
		for(int i = 0; i < list.getSize(); i++){
			Arco arco =  (Arco) list.get(i);
			K llave =  (K) arco.darNodoFin().darId();
			if(llave.equals(fin)){
				posicion = i;
				respuesta = arco;
			}
		}
		if(posicion != -1){
			list.deleteAtK(posicion);
			adj.put(inicio, list);
			eliminarContrario(fin, inicio);
		}
		
		return respuesta;
		
				
	}

	@Override
	public Nodo<K> buscarNodo(K id) {
		//TODO implementar
		Nodo respuesta = null;
		for(int i = 0; i < nodos.length; i++){
			if(nodos[i].darId().equals(id)){
				respuesta = nodos[i];
				break;
			}
		}
		return respuesta;
	}

	@Override
	public Arco<K,E>[] darArcosOrigen(K id) {
		//TODO implementar
		Lista lista = adj.get(id);
		if(lista != null){
		Arco[] respuesta = new Arco[lista.getSize()];
		for(int i = 0; i < lista.getSize(); i++){
			respuesta[i] = (Arco) lista.get(i);
		}
		
		return respuesta;
		}
		else{
			Arco respuesta[] = new Arco[0];
			return respuesta;
		}
	}

	@Override
	public Arco<K,E>[] darArcosDestino(K id) {
		//TODO implementar
		Lista <Arco> paraResponder = new Lista();
		for(int i = 0; i < nodos.length; i++){
			Nodo aRevisar = nodos[i];
			K llaveNodo = (K) aRevisar.darId();
			Lista temp = adj.get(llaveNodo);
			if(temp != null){
				for(int j = 0; j < temp.getSize(); j++){
					Arco arco = (Arco) temp.get(j);
					if(arco.darNodoFin().darId().equals(id)){
						paraResponder.add(arco);
					}
				}
			}
		}
		Arco[] respuesta = new Arco[paraResponder.getSize()];
		for(int k = 0; k < paraResponder.getSize(); k++){
			respuesta[k] = paraResponder.get(k);
		}
		return respuesta;
	}
	
	private void eliminarContrario(K inicio, K fin){
		
		Lista list = adj.get(inicio);
		int posicion = -1;
		for(int i = 0; i < list.getSize(); i++){
			Arco arco =  (Arco) list.get(i);
			K llave =  (K) arco.darNodoFin().darId();
			if(llave.equals(fin)){
				posicion = i;
			}
		}
		if(posicion != -1){
			list.deleteAtK(posicion);
			adj.put(inicio, list);
		}
	}
	
	public void setNumNodos(int n){
		nodos = new Nodo[n];
	}

}
