�Para qu� sirve el HashMap adj?
El Hash Map adj contiene las listas de adyacencia de cada Nodo, es decir
los arcos que van de un nodo a otro. Esta estructura permite realizar
b�squedas sobre el grafo, para verificar conectividad, hallar los caminos
mas cortos y mas largos de un nodo a otro. En conclusi�n el Hash Map adj
permite simplificar la implementaci�n del grafo no dirigido.